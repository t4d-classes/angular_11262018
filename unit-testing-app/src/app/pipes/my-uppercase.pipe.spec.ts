import { MyUppercasePipe } from './my-uppercase.pipe';

describe('MyUppercasePipe', () => {
  it('create an instance', () => {

    const pipe = new MyUppercasePipe();

    const actual = pipe.transform('test');

    expect(actual).toBe('TEST');

  });
});
