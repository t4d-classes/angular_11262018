import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';

import { ColorHomeComponent } from './components/color-home/color-home.component';
import { ColorFormComponent } from './components/color-form/color-form.component';
import { ColorListComponent } from './components/color-list/color-list.component';
import { ColorListItemComponent } from './components/color-list-item/color-list-item.component';
import { PersonFormComponent } from './components/person-form/person-form.component';

@NgModule({
  declarations: [
    ColorHomeComponent,
    ColorFormComponent,
    ColorListComponent,
    ColorListItemComponent,
    PersonFormComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    ColorHomeComponent,
    PersonFormComponent,
  ],
})
export class ColorToolModule { }
