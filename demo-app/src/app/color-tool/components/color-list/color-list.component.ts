import { Component, OnInit, Input, Output, EventEmitter, DoCheck, ChangeDetectionStrategy } from '@angular/core';

import { Color } from '../../models/color';

@Component({
  selector: 'color-list',
  templateUrl: './color-list.component.html',
  styleUrls: ['./color-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ColorListComponent implements OnInit, DoCheck {

  @Input()
  colors: Color[];

  @Output()
  deleteColor = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  ngDoCheck() {
    // console.log('color-list change detection');
  }

  doDeleteColor(colorId: number) {
    this.deleteColor.emit(colorId);
  }

}
