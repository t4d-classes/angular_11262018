import { RouterModule, Routes } from '@angular/router';

import { AppHomeComponent } from './components/app-home/app-home.component';

const routes: Routes = [
  { path: 'home', component: AppHomeComponent },
  { path: 'company', loadChildren: './company-tool/company-tool.module#CompanyToolModule' },
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  // { path: '**', component: PageNotFoundComponent },
];

export const AppRoutingModule = RouterModule.forRoot(routes);

