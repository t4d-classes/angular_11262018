import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject } from 'rxjs';
import { debounceTime, switchMap, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

interface Person {
  id: number;
  name: string;
  ssn: string;
}

@Component({
  selector: 'typeahead-demo',
  templateUrl: './typeahead-demo.component.html',
  styleUrls: ['./typeahead-demo.component.css']
})
export class TypeaheadDemoComponent implements OnInit {

  ssnSearchInput = new FormControl('');

  ssnSearch$ = new BehaviorSubject<string>('');

  ssnResults$: Observable<string[]>;

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    this.ssnResults$ = this.ssnSearch$.pipe(
      debounceTime(500),
      switchMap(ssnSearchValue =>
        this.httpClient.get<Person[]>('http://localhost:4250/people?ssn_like=' + ssnSearchValue)),
      map(people => people.map(person => person.name + '(!!' + person.ssn + ')')),
    );
  }

  doSSNSearch() {
    this.ssnSearch$.next(this.ssnSearchInput.value);
  }

}
