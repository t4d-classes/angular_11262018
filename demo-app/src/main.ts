// import { Observable, Observer, Subscription, interval, of, concat, merge } from 'rxjs';
// import { map, take, filter, delay, concat, merge } from 'rxjs/operators';

// const nums1 = of(1, 2, 3, 4, 5);
// const nums2 = of(10, 20, 30, 40, 50);

// nums1.pipe(concat(nums2)).subscribe(x => console.log(x));

// const nums3 = concat(nums1, nums2);

// const nums1 = interval(500);

// const nums2 = interval(1000).pipe(map(x => x * 10));

// nums1.pipe(merge(nums2)).subscribe(x => console.log(x));

// interval(500).pipe(
//   delay(3000),
//   // filter((n: number) => n > 5),
//   map((n: number) => n * 2),
//   take(5),
// ).subscribe(num => console.log(num));

// const ops = [
//   filter((n: number) => n > 5),
//   map((n: number) => n * 2),
//   take(5)
// ];

// const nums = interval(500);

// nums.pipe.apply(nums, ops).subscribe(num => console.log(num));


// const nums: Observable<number> = Observable.create( (observer: Observer<number>) => {
//   let counter = 0;

//   const h = setInterval(() => {
//     counter++;
//     if (counter > 5) {
//       observer.error('too high');
//     }
//     observer.next(counter);
//   }, 500);

//   setTimeout(() => {
//     clearInterval(h);
//     observer.complete();
//   }, 5000);

// } );

// const numsSubscription: Subscription = nums.subscribe(num => {
//   console.log('result: ', num);
// }, err => console.log(err), () => { console.log('all done'); });

// setTimeout(() => {
//   numsSubscription.unsubscribe();
// }, 5000);



// const p = new Promise(resolve => {
//   console.log('called promise function');
//   resolve('test');
// });

// let thenCounter = 0;

// p.then(result => { thenCounter++; console.log('result: ', result, 'then counter: ', thenCounter); });
// p.then(result => { thenCounter++; console.log('result: ', result, 'then counter: ', thenCounter); });


import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
