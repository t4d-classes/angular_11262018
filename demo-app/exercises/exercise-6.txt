Exercise #6

1. Create a new component named edit-car-row. The new component will provide edit field for the make, model, year, color, and price field. The id should be displayed but is not editable.

2. Add a new button to the view-car-row component, the label of the new button is Edit. When clicked the row will change from view-car-row to edit-car-row. Only one row is editable at a time.

To switch between edit row and view row consider using ngIf

3. The edit-car-row will have two buttons: Save and Cancel. When save is clicked, the new data typed into the fields should immutably replace the original in the array. When the cancel button is clicked, the data should not be saved.

4. When clicking the save, cancel, delete or add car button the editable should change back to a view row.